import React, { Component } from "react";
import Card from "react-md/lib/Cards/Card";
import CardText from "react-md/lib/Cards/CardText";
import UserLinks from "../UserLinks/UserLinks";
import config from "../../../data/SiteConfig";
import "./About.scss";

class About extends Component {
  render() {
    return (
      <div className="about-container md-grid mobile-fix">
        <Card className="md-grid md-cell--8">
          <div className="about-wrapper">
            <img
              src={config.userAvatar}
              className="about-img"
              alt={config.userName}
            />
            <CardText>
              <h2>Salie Lim</h2>
              <h3>Frontend Developer</h3>
              <li>Enjoy crafting minimalistic UI interfaces.</li>
              <li>Program mainly in JavaScript with React.js / JAMstack.</li>
              <li>Work with backend technologies like Ruby on Rails and Node.js too.</li>
              <li>Friendly introvert, contact me for a chat.</li>
              <br />

              <table>
                <tbody>
                  <tr>
                    <td>
                      <b>Languages &amp; Frameworks</b>
                    </td>
                    <td colspan="3">HTML, CSS, Javascript, React.js, AngularJS, Node.js, Ruby on Rails, jQuery</td>
                  </tr>
                  <tr>
                    <td>
                      <b>Databases</b>
                    </td>
                    <td colspan="3">MongoDB, mySQL, SQLite3, PostgreSQL</td>
                  </tr>
                  <tr>
                    <td>
                      <b>Servers</b>
                    </td>
                    <td colspan="3">Heroku, AWS</td>
                  </tr>
                  <tr>
                    <td>
                      <b>Testing</b>
                    </td>
                    <td colspan="3">TDD, MiniTest</td>
                  </tr>
                  <tr>
                    <td>
                      <b>Web & SEO Tools</b>
                    </td>
                    <td colspan="3">Google Analytics, Google Webmaster Tools, Google Keywords Planner, Screaming Frog SEO</td>
                  </tr>
                  <tr>
                    <td>
                      <b>Courses &amp; Certifications</b>
                    </td>
                    <td colspan="3">National University of Singapore Full Stack Foundation, freeCodeCamp Front End Development Certification, TechLadies
        Ruby on Rails Bootcamp</td>
                  </tr>
                </tbody>
              </table>
            </CardText>
            <UserLinks labeled config={config} />
          </div>
        </Card>
      </div>
    );
  }
}

export default About;
