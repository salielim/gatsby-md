import React, { Component } from "react";
import Card from "react-md/lib/Cards/Card";
import CardText from "react-md/lib/Cards/CardText";
import TextField from 'react-md/lib/TextFields';
import UserLinks from "../UserLinks/UserLinks";
import config from "../../../data/SiteConfig";
import "./Hire.scss";

class Hire extends Component {
  render() {
    return (
      <div className="about-container md-grid mobile-fix">
        <Card className="md-grid md-cell--8">
          <div className="about-wrapper">
            <CardText>
              <h2>Hire Me</h2>
              <p>I build <b>Websites and Progressive Web Apps (PWA)</b> to help businesses establish an internet presence and become more efficient using software. I also provide <b>Search Engine Optimisation (SEO)</b> consultation to help websites increase organic traffic.</p>
              <br />
              <br />
              <h4>Please fill this form and I’ll get back to you.</h4>
              <br />
              Your Name <TextField id="name" />
              <br />
              Your Email <TextField id="name" />
              <br />
              Your Website, if you have one <TextField id="name" />
              <br />
              Your Location <TextField id="name" />
              <br />
              What I can help you with <TextField id="name" />
              <br />
              What are the primary objectives for the project? <TextField id="name" />
              <br />
              Please provide any additional details for the project <TextField id="name" />
              <br />
              Timeline <TextField id="name" />
              <br />
              Budget <TextField id="name" />
              <br />
            </CardText>
          </div>
        </Card>
      </div>
    );
  }
}

export default Hire;