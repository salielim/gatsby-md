import React from "react";
import FontIcon from "react-md/lib/FontIcons";
import Link from "gatsby-link";

function GetNavList(config) {
  const NavList = [
    {
      primaryText: "Home",
      leftIcon: <FontIcon>home</FontIcon>,
      component: Link,
      to: "/"
    },
    {
      primaryText: "About",
      leftIcon: <FontIcon>person</FontIcon>,
      component: Link,
      to: "/about/"
    },
    {
      primaryText: "Portfolio",
      leftIcon: <FontIcon>laptop</FontIcon>,
      component: Link,
      to: "/portfolio/"
    },
    {
      primaryText: "Blog",
      leftIcon: <FontIcon>edit</FontIcon>,
      component: Link,
      to: "/blog/"
    },
    {
      primaryText: "Hire Me",
      leftIcon: <FontIcon>work</FontIcon>,
      component: Link,
      to: "/hire/"
    },
    {
      divider: true
    }
  ];

  return NavList;
}
export default GetNavList;
