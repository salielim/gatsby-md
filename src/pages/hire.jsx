import React, { Component } from "react";
import Helmet from "react-helmet";
import Hire from "../components/Hire/Hire";
import config from "../../data/SiteConfig";

class HirePage extends Component {
  render() {
    return (
      <div className="about-container">
        <Helmet>
          <title>{`Hire | ${config.siteTitle}`}</title>
          <link rel="canonical" href={`${config.siteUrl}/hire/`} />
        </Helmet>
        <Hire />
      </div>
    );
  }
}

export default HirePage;