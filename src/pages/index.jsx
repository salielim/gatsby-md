import React from "react";
import Helmet from "react-helmet";
import PostListing from "../components/PostListing/PostListing";
import SEO from "../components/SEO/SEO";
import config from "../../data/SiteConfig";

class Index extends React.Component {
  render() {
    const postEdges = this.props.data.allMarkdownRemark.edges;
    return (
      <div className="intro-container">
        <h1 className="primary-typo">Salie Lim</h1>
        <h2 className="secondary-typo">Frontend Developer</h2>
        <h3 className="tertiary-typo">Websites and Progessive Web Apps</h3>
        <button>View My Work</button>
        <button>Read My Blog</button>
      </div>
    );
  }
}

export default Index;